<?php include_once 'partials/_header.php'; ?>

    <div class="jumbotron">
        <div class="container text-center">
            <h1 class="display-3">CMS Test</h1>
            <p>
                This site contains two type of pages, category and content pages <br>
                Each category page contains list fo links to content pages <br>
                Each content page contains links to other content pages <br>
                All pages have link to category pages
            </p>
            <p><a class="btn btn-primary btn-lg" href="<?php echo CATEGORIES_PATH ?>" role="button">All Categories »</a></p>
        </div>
    </div>

<?php include_once 'partials/_footer.php'; ?>
