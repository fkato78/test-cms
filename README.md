### SIMPLE TEST CMS
**Configurations :**
- in the config.php file just change the hostname at lines(5, 10)
```php
if(!defined('BASE_URL')) {
    define("BASE_URL", "/test.two/"); // line 5
}


if(!defined('ROOT_PATH')) {
    define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"] . "/test.two/"); // line 10
}
```