<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="<?php echo BASE_URL; ?>">CMS</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav">
                <li class="nav-item <?php echo $_SERVER["REQUEST_URI"] == CATEGORIES_PATH ? "active" : "" ?>">
                    <a class="nav-link" href="<?php echo CATEGORIES_PATH ?>">Categories<span class="sr-only"></span></a>
                </li>
            </ul>
        </div>
    </div>

</nav>

