<?php
/** Application Constants */
// Base site url
if(!defined('BASE_URL')) {
    define("BASE_URL", "/test.two/");
}

// Root path
if(!defined('ROOT_PATH')) {
    define("ROOT_PATH", $_SERVER["DOCUMENT_ROOT"] . "/test.two/");
}

// Categories path
if(!defined('CATEGORIES_PATH')) {
    define("CATEGORIES_PATH", BASE_URL. "categories");
}

// Error 404 page path
if(!defined('ERROR_404')) {
    define("ERROR_404", BASE_URL . "404.php");
}

/** Helper function  */
include_once "helpers.php";