<?php
/**
 * ALL CATEGORIES PAGE
 */

include_once 'partials/_header.php'; ?>

<div class="container mt-5">
    <h1 class="text-center mb-4">Categories page </h1>
    <hr>
    <div class="row mt-6">
        <?php
        $data = generate_categories();
        if (sizeof($data) > 0):
            $counter = 1;
            foreach ($data as $category):?>
                <div class="col-md-6">
                    <h2><?php echo ucfirst($category['title']) ?? ""; ?></h2>
                    <p> <?php echo $category['body'] ?? ""; ?> </p>
                    <?php $safeUrl = filter_var(CATEGORIES_PATH . "/" . generateSeoURL($category['title']), FILTER_SANITIZE_URL) ?>
                    <p><a class="btn btn-secondary" href="<?php echo $safeUrl; ?>" role="button">View details »</a></p>
                </div>
                <?php if ($counter % 2 == 0 && $counter < sizeof($data)): ?>
                    <hr class="mt-4" style="width: 100%">
                <?php endif; ?>
                <?php $counter++; endforeach; else: ?>
            <h3 class="text-center">There are no categories</h3>
        <?php endif; ?>
    </div>
</div>

<?php include_once 'partials/_footer.php'; ?>

