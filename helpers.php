<?php
/**
 * @param string $fileName
 * @return array
 * Generate array of categories from source json file
 */
function generate_categories(string $fileName = "./source.json"): array {
    // initial array of data
    $jsonData = [];
    // if the source file exists get it and convert ti array
    if(file_exists($fileName)){
        $json = file_get_contents($fileName);
        $jsonData = json_decode($json, true);
    }
    return $jsonData;
}


/**
 * @param $string
 * @return string
 */
function generateSeoURL(string $string): string {
    // Trim String
    $string = trim($string);
    //Unwanted:  {UPPERCASE} ; / ? : @ & = + $ , . ! ~ * ' ( )
    $string = strtolower($string);
    //Strip any unwanted characters
    $string = preg_replace("/[^a-z0-9_\s-]/", "", $string);
    // Clean multiple dashes or whitespaces
    $string = preg_replace("/[\s-]+/", " ", $string);
    //Convert whitespaces and underscore to dash
    $string = preg_replace("/[\s_]/", "-", $string);

    return $string;
}

/**
 * @param array $category
 * @return array
 * Get all contents of category
 */
function displayContentsOfCategory(array $category): array {
    $categoryContents = [];
    if(is_array($category) && sizeof($category) > 0 ){
        foreach ($category as $key => $contents){
            $categoryContents = $contents;
        }
    }
    return $categoryContents;
}

/**
 * @return array
 * Current category
 */
function getCurrentCategory(): array {
    // Init Data
    $currentCategory = [];
    // Cycle through all categories(json source file)
    $data = generate_categories();
    foreach ($data as $key => $category) {
        // Title of the category the same from url
        if ($_GET['title'] == generateSeoURL($category['title'])) {
            $currentCategory = $category;
        }
    }
    return $currentCategory;
}

/**
 * @param string $slug
 * @return string
 * Generate title from given slug
 */
function generateTitleFromSlug(string $slug) : string{
    return ucwords(str_replace("-", " ", $slug));
}

/**
 * @return array
 * Generate random categories and their contents
 */
function displayRandomContents() {
    // Init variables(Always)
    $contents = [];
    $pure = [];
    // cycle through all categories(json source file)
    foreach (generate_categories() as $key => $value){
        $contents[] = $value;
        array_push($pure, $value);
    }
    // random the data
    shuffle($pure);
    // limit the data
    $randomContents = array_slice($pure, 0, 2);

    return $randomContents;
}

/**
 * @param string $location
 * @return string
 */
function redirect(string $location): string{
    header("Location:" . $location);
    exit;
}

