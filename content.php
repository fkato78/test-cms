<?php
/**
 * SINGLE CONTENT PAGE
 */

include_once 'partials/_header.php';

// Load the current category
$currentCategory = getCurrentCategory();

// Get the content slug from the url
$url = explode('/', trim(parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), '/'), 5);
$contentSlug = $url[4];
$randomContents = [];
$string = "";
foreach ($currentCategory['contents'] as $category){
    $string .=" ". $category['title'] ;
}
/** If U asked fro wrong content of category */
if(!strpos(strtolower($string),strtolower(generateTitleFromSlug($contentSlug)))){
    redirect(ERROR_404);
}
?>
    <div class="container mt-5">
        <h1 class="text-center mb-4"><?php echo generateTitleFromSlug($contentSlug) ?></h1>
        <hr>
        <div class="row mt-4">
            <!-- Display the body of the current content -->
            <?php if (is_array($currentCategory) && sizeof($currentCategory) > 0): ?>
                <?php $contents = displayContentsOfCategory($currentCategory) ?>
                <?php foreach ($contents as $key => $content): ?>
                        <?php if (generateSeoURL($content['title']) === $contentSlug): ?>
                        <div class="jumbotron">
                            <p class="lead"><?php echo $content['body'] ?></p>
                        </div>
                    <?php endif; ?>

                <?php endforeach; ?>
            <?php endif; ?>
        </div>
        <ul class="list-group">
            <?php
            // List of random contents from random categories
            $randomContents = displayRandomContents();
            foreach ($randomContents as $key => $randomContent):
                foreach ($randomContent['contents'] as $content):?>
                    <li class="list-group-item">
                        <a href="<?php echo CATEGORIES_PATH . "/" . generateSeoURL($randomContent['title']) . "/content/" . generateSeoURL($content['title']) ?>">
                            <?php echo $content['title'] ?>
                        </a>
                    </li>
                <?php endforeach;
            endforeach;
            ?>
        </ul>
    </div>


<?php include_once 'partials/_footer.php'; ?>