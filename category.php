<?php
/**
 * SINGLE CATEGORY PAGE
 */
include_once 'partials/_header.php';
$currentCategory = getCurrentCategory();
?>
    <div class="container mt-5">
    <h1 class="text-center"><?php echo ucfirst($currentCategory['title']); ?></h1>
    <hr>
<?php if (isset($currentCategory['contents'])):?>
<?php foreach ($currentCategory['contents'] as $key => $content): ?>
    <?php $safeContentUrl = filter_var(CATEGORIES_PATH . "/" . generateSeoURL($currentCategory['title']) . "/content/" . generateSeoURL($content['title']), FILTER_SANITIZE_URL) ?>
    <div class="card">
        <div class="card-header"><?php echo ucfirst($key); ?></div>
        <div class="card-body">
            <h5 class="card-title"><?php echo ucfirst($content['title']); ?></h5>
            <p class="card-text"><?php echo ucfirst($content['body']); ?></p>
            <a href="<?php echo $safeContentUrl; ?>" class="btn btn-primary">See More</a>
        </div>
    </div>
<?php endforeach; ?>
<!-- If wrong category has been asked -->
<?php else:
redirect(ERROR_404);
endif; ?>

<?php include_once 'partials/_footer.php'; ?>