<?php include_once 'partials/_header.php';?>

<div class="container mt-5">
    <h1 class="text-center mb-4 text-danger">OOps ! Page not found</h1>
    <a href="<?php echo BASE_URL ?>" class="btn btn-dark btn-block">Home</a>
</div>


<?php include_once 'partials/_footer.php'; ?>
